// variables y constantes

const liA = document.querySelector('#numeroA');
const liB = document.querySelector('#numeroB');
const divSalida = document.querySelector('#salida');

let a = 0;
let b = 0;
let salida = "";

// entradas

a = Number(prompt("Introduce el primer numero", 0));
b = Number(prompt("Introduce el segundo numero", 0));

// procesamiento
if (a > b) {
    //salida="El mayor es: "+a;
    salida = `El mayor es ${a}`;
} else {
    salida = `El mayor es ${b}`;
}

// salidas

liA.innerHTML += a;
liB.innerHTML += b;

divSalida.innerHTML = salida;

console.log(salida);
