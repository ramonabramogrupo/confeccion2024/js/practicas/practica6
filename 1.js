// variables y constantes

let numero1 = 0;
let numero2 = 0;
let suma = 0;
let resta = 0;
let multiplicacion = 0;
let division = 0;
let raiz = 0;
let potencia = 0;

// constantes

const liSuma = document.querySelector('#suma');
const liResta = document.querySelector('#resta');
const liMultiplicacion = document.querySelector('#multiplicacion');
const liDivision = document.querySelector('#division');
const liRaiz = document.querySelector('#raiz');
const liPotencia = document.querySelector('#potencia');
const liNumero1 = document.querySelector('#numero1');
const liNumero2 = document.querySelector('#numero2');


// entradas

numero1 = Number(prompt("Introduce el primer numero", 4));
numero2 = Number(prompt("Introduce el primer numero", 2));

// procesamiento

suma = numero1 + numero2;
resta = numero1 - numero2;
multiplicacion = numero1 * numero2;
division = numero1 / numero2;
raiz = Math.sqrt(numero1);
potencia = numero1 ** numero2;

// salida

liNumero1.innerHTML += numero1;
liNumero2.innerHTML += numero2;
liSuma.innerHTML += suma;
liResta.innerHTML += resta;
liMultiplicacion.innerHTML += multiplicacion;
liDivision.innerHTML += division;
liRaiz.innerHTML += raiz;
liPotencia.innerHTML += potencia;

// salida en consola

console.log("numero1: " + numero1);
console.log("numero2: " + numero2);
console.log("suma: " + suma);
console.log("resta: " + resta);
console.log("multiplicacion: " + multiplicacion);
console.log("division: " + division);
console.log("raiz: " + raiz);
console.log("potencia: " + potencia);

// utilizar el operador ${} para concatenar variables

console.log(`numero1: ${numero1}`);
console.log(`numero2: ${numero2}`);
console.log(`suma: ${suma}`);
console.log(`resta: ${resta}`);
console.log(`multiplicacion: ${multiplicacion}`);
console.log(`division: ${division}`);
console.log(`raiz: ${raiz}`);
console.log(`potencia: ${potencia}`);

